#!/bin/bash

sls info | grep https > .env

sed -i -e 's/  POST - //g' .env

sed -i -e 's/  GET - //g' .env

# Append EV keys to API Endpoints
sed -i -e '/login/ s/^/VUE_APP_LOGIN_URL=/' .env

sed -i -e '/checkin/ s/^/VUE_APP_CHECK_IN_URL=/' .env

sed -i -e '/statuses/ s/^/VUE_APP_GET_CHECKINS_URL=/' .env