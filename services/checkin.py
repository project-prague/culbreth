import os
import boto3  
import json
# import ast

sqs = boto3.resource('sqs')

ddb_q = sqs.Queue(os.environ['CULBRETH_CHECK_IN_Q'])

def lambda_handler(event, context):
        response = ddb_q.send_message(
            MessageBody=json.dumps(event)
            # DelaySeconds=30,
        )
        print(event)
        return(response)
        print("The first check-in function has run")