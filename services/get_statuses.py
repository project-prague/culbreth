import os
import boto3
from boto3.dynamodb.conditions import Key, Attr
import json

#How do we ensure encryption in tranit? PrivateLink? VPC / IG?

#import base64
#import hashlib

# Init DDB
dynamodb = boto3.resource('dynamodb')

#Init Table
table = dynamodb.Table(os.environ['CULBRETH_CHECK_IN_TABLE'])

'''
Since this is going to be a GET request from the client, this function can be
refactored to take a number of check-ins to fetch as part of the event, removing
the need for redundant function layers. In fact, querying as a whole is on hold
until we figure out the correct table schema. For now we're returning all items
or items by agent name, etc.
'''

def lambda_handler(event, context):

    results = table.scan()
    data = results['Items']
    '''
    d = {}
    for x in data:
        d = {
            ['GUID'],
            ['agent'],
            ['timeStamp'],
            ['checkInTime']
        }
    '''
    # print(data)
    return(data)