# TODO: Add proper logging and timestamps to the custom error messages
import os
import boto3
from boto3.dynamodb.conditions import Key, Attr
# from botocore.exceptions import ClientError
import json
# import base64
# import hashlib
# import pickle

dynamodb = boto3.resource('dynamodb')

table = dynamodb.Table(os.environ['CULBRETH_USER_TABLE'])

def lambda_handler(event, context):

    print(event)
    prov_user = event['body']['body']['username']
    chk_for_user = table.query(
        KeyConditionExpression=Key('username').eq(prov_user)
    )

    user_chk_result = chk_for_user['Count']

    if user_chk_result == 0:
        print("ERROR: USER" + prov_user + "DOES NOT EXIST IN DATABASE")
        return("There was an error with your credentials. Please check them or use the Forgot Password link")
        # TODO: Replace this with the non-zero error code
    else:
        print("User Exists in the Database. Proceeding with Auth Check...")

        db_userID = chk_for_user['Items'][0]['id']
        db_pw = chk_for_user['Items'][0]['password']
        db_role = chk_for_user['Items'][0]['role']
        user_pw = event['body']['body']['password']

        if db_pw == user_pw:
            print("Password is confirmed correct. Checking role...")
            
            if db_role == 'admin':
                return 2
            else:
                return 0
        
        else:
            print("Password is incorrect. Logging attempt to FailedLogins Table.")
            print("5 more attempts in the next 10 minutes will lock the account.")
            print(
                "The user, Operations Team, and other designated principals will be notified")
            # TODO: The things in the print messages above (for both cases)
            return 1