import os
import boto3
import json
import ast

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['CULBRETH_CHECK_IN_TABLE'])

def lambda_handler(event, context):
    ''' This print event outputs a single quotes json string that is 
    identical to a typical json entry, minus the single quotes.
    print(event)
    '''

    # Keeping this snippet for reference
    # message = event['Records'][0]['Sqs']['Message']
    
    '''
    This snippet just wraps the above output in double quotes,
    technically making it a string, but not json
    raw_message = event['Records'][0]['body']
    # print(raw_message)
    json_string = json.dumps(raw_message)
    print(json_string)
    '''
    
    '''*** this pretty prints well formatted json, but i want the object
    for the ddb put
    for record in event['Records']:
        print(record['body'])
    '''
    
    '''
    This converts the message to a dict and gives us access to the keys!
    It also means that if a dict is the only way to get access to the actual
    values then we probably didn't need to convert things to single string
    in the initial check in function - WHOOPS XD XD XD XD
    '''

    # print(event)
    msg = event['Records'][0]['body']
    lit_msg = ast.literal_eval(msg)
    # print(type(lit_msg))

    agent_name = lit_msg['body']['agent']
    unique_id = lit_msg['body']['GUID']
    unix_time = lit_msg['body']['timeStamp']
    agent_status = lit_msg['body']['status']
    
    table.put_item(
       Item={
            'GUID': unique_id,
            'agent': agent_name,
            'timeStamp': unix_time,
            'status': agent_status
        }
    )
    print("The DDB processing function has run")