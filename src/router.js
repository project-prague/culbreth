import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import CheckIn from './components/Checkin.vue'
import Register from './views/Register.vue'
import Dash from './views/Dash.vue'
// import NotFound from './views/NotFound.vue'
import VueRouter from 'vue-router';
import store from './store.js';

Vue.use(Router)

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,

  routes: [{
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true,
        staffAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: "/checkin",
      name: "checkin",
      component: CheckIn,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/dash",
      name: "dash",
      component: Dash,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth) {
    const confirmCreds = store.getters.checkAuth
    if(confirmCreds === false) {
      next({name:'login'})
    }
    else if(to.meta.adminAuth) {
    const adminCheck = store.getters.checkAdmin
    if(adminCheck === true) {
      next({name: 'dash'})
    }else {
      next('/home')
    }
  } else if(to.meta.staffAuth) {
    const staffChk = store.getters.checkAdmin
    if(staffChk === false) {
      next()
    }else {
      // console.log('Admin Route Engaged')
      next('/dash')
    }
  }
  }else {
  next()
  }
})

/*
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.checkAuth) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})
*/

export default router;