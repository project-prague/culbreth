import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoggedIn: false,
    user: '',
    checkins: [],
    isAdmin: false,
    reports: []
  },
  getters: {
    checkAuth: state => {
      return state.isLoggedIn;
    },
    checkAdmin: state => {
      return state.isAdmin;
    },
    agentStatusCheck: state => {
      return state.checkins;
    },
    currentUser: state => {
      return state.user;
    },
    viewReports: state => {
      return state.reports;
    }
  },
  mutations: {
    SET_LOGIN_STATUS(state) {
      state.isLoggedIn = !state.isLoggedIn;
    },
    SET_USER(state, username) {
      state.user = username
    },
    SET_CHECKS(state, data) {
      // state.checkins.splice(0, state.checkins.length);
      state.checkins.push(data);
      state.checkins = [].concat.apply([], state.checkins);
      // console.log(data);
    },
    SET_ADMIN(state) {
      state.isAdmin = true;
    },
    UNSET_ADMIN(state) {
      state.isAdmin = false;
    },
    SUBMIT_REPORT(state, report) {
      state.newReport = report
      // console.log(state.newReport);
    },
    SCAN_REPORTS(state, data) {
      state.reports.push(data);
      state.reports = [].concat.apply([], state.reports)
    }
  },
  actions: {}
});