# Culbreth

_NB: For AWS Deployments via the GitLab CI/CD Pipeline, AWS credentials are required as environment variables. During the portfolio review process, this repository will be public, hence making those variables open to th world. To avoid this scenario, I have forked this repo to a private repository that will handle the deployment of the environments. With that i mind, this repository will serve as a documentation of the development workflow, project requirements, and application code - despite the deployment being handled by a different repo. As the project evolves this is a potential improvement, perhaps by using Git hooks or a dedicated instance for deployment._

**Culbreth is the first prototype application as part of my professional portfolio in preperation for upcoming job applications. It is a work in progress and is meant to showcase my abilities in cloud architecture, my next professional move.**

**For a detailed walkthrough of the project, please check out the [YouTube video](https://youtu.be/cS3o-yZLEWc)**

<!-- vscode-markdown-toc -->
1. [Project Overview](#ProjectOverview)
2. [Technical Details](#TechnicalDetails)
   - 2.1 - [AWS Services Used](#AWSServicesUsed)
   - 2.2 - [Additional Techincal Details](#AddTechDetails)
3. [Application Workflow](#ApplicationWorkflow:)
4. [CI/CD](#CICD)
   - 4.1 [CI/CD Workflow](#CICDWorkflow)
5.  [Reflections, Future Improvements, Etc](#ReflectionsFutureImprovementsEtc)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='ProjectOverview'></a>Project Overview

 - _[Video](https://youtu.be/cS3o-yZLEWc)_

Culbreth is part of "Prague", a suite of cloud applications for a mock national security agency. The entire suite is meant to support the operational needs of agents in the field in varying capacities. In this mock environment, Culbreth is a fake multi-national consulting corporation. This cover will allow agents deployed abroad to hide in plain sight while observing and reporting on required activities and personnel.

The initial functionality of the application will be allowing agents to "Check-In", providing staff in HQ some insight into the state of deployed agents and whether additional measures need to be taken to ensure both the safety of agents and the integrity of the mission.

As such, the frontend of the web application will prompt agents to provide a check-in status: Green, Yellow, or Red. Each status will eventually be linked to automated cloud operations to support the necessary actions for each reported status. For example, a Red check-in should automatically notify the required personnel that an extraction operation is imminent.

###  2.  <a name='TechnicalDetails'></a>Technical Details

####  2.1 -  <a name='AWSServicesUsed'></a>AWS Services Used

1. API Gateway - Routing of application requests to appropriate sources (i.e. /login, /checkin)
2. Lambda - Serverless event processing (login, checkin, kinesis stream parsing, log delivery)
3. S3 - Hosting of static website assets and log files
4. CloudFront - CDN for faster global delivery of the application
5. Simple Queue Service - Queues to hold checkin events for Lambda processing
6. Kinesis - Capturing of checkin events for observability via Lambda processing and S3 log delivery
7. CloudWatch - Select CW Log events are sent to Kinesis for parsing via Lambda and delivery to S3 for app observability
8. Route53 - Registration of www.culbrethpartners.com (for production website), and DNS management of production website.
9. AWS Certificate Manager - SSL certificate management for production website
10. AWS Systems Manager - Parameter Store to hold secret values for use by the Serverless framework. This also prevents the necessity to hardcode secrets into the application, or commit them to source control.
11. DynamoDB - On demand, No-SQL Database to hold user information and check-in data.

####  2.2 -  <a name='AddTechDetails'></a>Additional Technical Details

- Frontend: VueJS, a powerful framework for building user interfaces. Requires the use of Javascript and NodeJS.
- Serverless Framework: This allows for automated deployment of Lambda functions with an Infrastructure as Code workflow, as the functions can be defined in YAML files. The Lambda functions for Culbreth are written in Python 3.6, exclusively.
- GitLab Pipelines: A robust framework for scripting deployment and other jobs based on events in the Git repository.
- Shell scripts: The use of an automated pipeline system like GitLab's means there is a need to automate certain processes for usability in development and deployment.
- AWS CLI: Using a CI/CD system like Gitlab, it's necessary to script AWS CLI jobs for various deployment and managements tasks. The CLI was also instrumental in deploying the Kinesis infrastucture necessary for establishing foundational observability. This appears to be due to current limitation in the AWS Kinesis console for these tasks.

###  3. <a name='ApplicationWorkflow:'></a>Application Workflow

For a detailed walkthrough of the application, I recommened watching the [YouTube video](https://youtu.be/cS3o-yZLEWc)
that details the project. The diagrams below may suffice, however\*

##### Login Flow

![Login Workflow](/diagrams/culbloginflow.png)

1. User accesses front-end via Route53 (prod) or CloudFront (dev). Prod still uses CF as a CDN.
2. They are served the frontend login assets.
3. The login attempt calls API Gateway, which triggers a Lambda function to check credentials on the user table.
4. If this call is successful, they are served the restricted frontend assets. Otherwise and error message is logged and they are restricted access.

##### Check In Flow

![Check-in Workflow](/diagrams/CulbrethCheckInFlow.png)

1. User accesses front-end via Route53 (prod) or CloudFront (dev). Prod still uses CF as a CDN.
2. They are served the frontend login assets, including the check-in workflow.
3. The checkin calls API Gateway, which triggers a Lambda function to send the check-in event to an SQS Queue.
4. The SQS Queue triggers a Lambda that processes the check-in event and sends it to DynamoDB.

##### Admin Flow

![Admin Workflow](/diagrams/CulbrethAdminFlow.png)

1. User accesses front-end via Route53 (prod) or CloudFront (dev). Prod still uses CF as a CDN.
2. They are served the frontend Admin assets, including the "Get Statuses" flow.
3. The "Get Statuses" buton calls API Gateway, which triggers a Lambda function to fetch all statuses from DDB.
4. This is displayed for the admin user via the UI.

##### Observability Flow

![Observe Workflow](/diagrams/CulbrethObservabilityFlow.png)

1. This follows the same flow as the "Check-In" flow from above for steps 1-3.
2. A Lambda function processes the check in from the queue.
3. The Lambda function sends the check in data to a Kinesis stream.
4. This stream triggers another Lambda function that processes the stream data.
5. The stream data is stored in a dediated logging S3 bucket for further analysis and ETL.

###  4. <a name='CICD'></a>CI/CD

 - *[Video](https://www.youtube.com/watch?v=cS3o-yZLEWc&t=1455s)*

A major requirement of this project is a robust CI/CD pipeline allowing for the rapid development, testing, and deployment of new features. GitLab's built in CI/CD functionality, in conjunction with the Serverless framework make this a solid reality. This is compounded by the fact that GitLab and Serverless allow for the use of environment variables, and Serverless allows explicit access (provided the invoking IAM Role has permissions) to the Parameter Store, eliminating the need for hard coding of secrets or commitment of secrets to source code.

####  4.1 <a name='CICDWorkflow'></a>CI/CD Workflow

##### Develop Branch

- Commits to develop start a pipeline that builds the Serverless backend via CloudFormation, then deploys the front end changes to the designated S3 bucket. This updates the CF distribution, updating the front end.
- The development region is Us-East-1.

###### Master Branch

- Commits to master that are TAGGED start a pipeline that builds the Serverless backend via CloudFormation, then deploys the front end changes to the designated S3 bucket. This updates the CF distribution, updating the front end.
- The Production CF distribution is linked to the Route53 record for [culbrethpartners.com](https://www.culbrethpartners.com/), updating the production front end for end users.
- The development region is Eu-West-1.

###### Feature Branches

- Feature branches will be developed locally after branching off of develop.
- Feature branches will be prefixed by "feature", followed by a description of the feature or the identifier of the GitLab issue that is being addressed.
- Developers can test features locally using the `npm run serve` command locally to test features, in conjunction with the `sls invoke local` command to mock AWS events locally without deploying the services.
- As the CI/CD pipeline gets more robust, tests will be required to pass for each feature before it can be merged with develop. This in turn will kick-off logic that allows for the manual approval of approved develop merges into production. A true CI/CD pipeline!

###  5. <a name='ReflectionsFutureImprovementsEtc'></a>Reflections, Future Improvements, Etc

To keep the README clean, I advise that you refer to this section of the [video walkthrough](https://www.youtube.com/watch?v=cS3o-yZLEWc&t=1829s) for more information on what is to come from this project, as well as what I learned. You'll be able to keep up to date via the Public GitLab issues that are created in this project.

James
